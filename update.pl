#!/usr/bin/perl -w
# Update all repos in repos/

use strict;
use warnings;
use Cwd;
use Data;

my $dir = getcwd;

sub write_summary {
	my $file = $dir . "/data/" . shift . ".data";
	my $track = shift;

	open DATA, ">$file";
	foreach (sort keys $track) {
		print DATA "$_ $track->{$_}\n";
	}
	close DATA;
}

sub git {
	my $repo = shift;
	chdir "$dir/repos/$repo";
	system "git pull";

	my %track = ();
	open (PIPE, "git log --date=short|");

	while (<PIPE>) {
		if ($_ =~ /^Date:[ ]*(\d{4})-(\d{2})-/) {
			$track{"$1-$2"} ++;
		}
	}
	close PIPE;
	chdir $dir;
	write_summary $repo, \%track;
}

sub hg {
	my $repo = shift;
	chdir "$dir/repos/$repo";
	system "hg pull";
	system "hg update";

	my %track = ();
	open (PIPE, 'hg log --template "{date|isodate}\\n"|');

	while (<PIPE>) {
		if ($_ =~ /^(\d{4})-(\d{2})-/) {
			$track{"$1-$2"} ++;
		}
	}
	close PIPE;
	chdir $dir;
	write_summary $repo, \%track;
}

sub svn {
	my $repo = shift;
	chdir "$dir/repos/$repo";
	system "svn up";

	my %track = ();
	open (PIPE, 'svn log --xml|');

	while (<PIPE>) {
		# yikes, what a hack
		if ($_ =~ /<date>(\d{4})-(\d{2})-\d{2}T/) {
			$track{"$1-$2"} ++;
		}
	}
	close PIPE;
	chdir $dir;
	write_summary $repo, \%track;
}

sub cvs {
	my $repo = shift;
	chdir "$dir/repos/$repo";
	system "cvs -q -z6 up -Pd";

	my %track = ();
	open (PIPE, 'cvsps -x|');

	while (<PIPE>) {
		# yikes, what a hack
		if ($_ =~ /^Date: (\d{4})\/(\d{2})/) {
			$track{"$1-$2"} ++;
		}
	}
	close PIPE;
	chdir $dir;
	write_summary $repo, \%track;
}

# if ARGV is nonempty, update only the repos in ARGV
my @repos = scalar(@ARGV) ? @ARGV : keys %Data::software;

foreach (sort @repos) {
	if (-d "repos/$_/.svn/") {
		print "Updating $_, which is a svn repo:\n";
		svn $_;
	} elsif (-d "repos/$_/CVS/") {
		print "Updating $_, which is a CVS repo:\n";
		cvs $_;
	} elsif (-d "repos/$_/.git/") {
		print "Updating $_, which is a git repo:\n";
		git $_;
	} elsif (-d "repos/$_/.hg/") {
		print "Updating $_, which is a mercurial repo:\n";
		hg $_;
	} else {
		print "Not sure what $_ is :(\n";
	}
}
