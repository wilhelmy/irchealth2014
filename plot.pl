#!/usr/bin/perl -w
use Data;

my $file = shift @ARGV;

unless ($file) {
	for (my $n = 0; ; $n++) {
		$file = "plot$n.png";
		last unless -f $file;
	}
}


my $hdr = <<EOF;
set xdata time
set timefmt "%Y-%m"
set xrange ["1999-01":"2014-08"]
set format x "%Y-%m"
#set logscale y 
set yrange [-30:200]
set grid

set terminal pngcairo size 1600,1600 enhanced font 'Verdana,10'
set output '$file'

EOF

open PLOT, "|gnuplot"
	or die "Unable to open $file: $!";

my %software = %Data::software;

my @tmp = ();
unshift @tmp, "\t'data/$_.data' u 1:2 smooth csplines title '$software{$_}'"
	foreach (sort keys %software); 

my $cat = join ", \\\n", @tmp;

printf PLOT "%s\nplot \\\n%s", $hdr, $cat;

close PLOT;

