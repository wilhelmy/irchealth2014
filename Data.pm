#!/usr/bin/perl -w

# this package contains shared data regarding irc repositories
package Data;

our %software = (
# Clients
"bitchx"        => "BitchX",
"chatzilla"     => "ChatZilla",
"hexchat"       => "HexChat",
"irssi"         => "Irssi",
"konversation"  => "Konversation",
"kvirc"         => "KVIrc",
"limechat"      => "LimeChat",
"quassel"       => "Quassel",
"smuxi"         => "Smuxi",
"textual"       => "Textual",
"weechat"       => "Weechat",
"xchat"         => "XChat",
"pidgin"        => "Pidgin",

# Bots
"eggdrop1.6"    => "Eggdrop 1.6",
"eggdrop1.8"    => "Eggdrop 1.8",

# ircds
"ratbox"        => "Ratbox ircd",
"hybrid"        => "Hybrid ircd",
#"ircu"         => "ircu",
"irc2.11"       => "IRCnet ircd",
"charybdis"     => "Charybdis ircd",

# TODO: Services
);

1
